window.addEventListener("load", () => {
  showServices();
});

async function showServices() {
  let services = [];

  try {
    const res = await fetch("services.json");
    services = await res.json();
  } catch (e) {
    alert("Services not found");
  }

  services.forEach((service) => {
    const { name, logo, cards } = service;
    createSection(name, logo, cards);
  });
}

function createSection(name, logo, cards) {
  const mainEl = document.querySelector("main");
  const sectionEl = e("section", mainEl);

  const headingEl = e("div", sectionEl, "heading");
  e("img", headingEl, "icon", "", { alt: "", src: logo });
  e("h2", headingEl, [], name);

  const cardsEl = e("div", sectionEl, "cards");
  cards.forEach((card) => {
    const { name, url, logo, description, authenticated } = card;
    createCard(name, url, logo, description, cardsEl, authenticated);
  });
}

function createCard(
  name,
  url,
  logo,
  description,
  parentEl,
  authenticated = false
) {
  const cardEl = e("a", parentEl, "card", "", { href: url });

  e("img", cardEl, "icon", "", { alt: `${name} logo`, src: logo });

  const descriptionEl = e("div", cardEl, "description");
  e("div", descriptionEl, "main", name);
  e("p", descriptionEl, "content", description);

  if (authenticated) {
    e("img", e("div", cardEl, "label"), [], "", {
      alt: "lock",
      src: "assets/lock.svg",
    });
  }
}

// create a new DOM element
function e(
  type,
  parent = document.body,
  className = [],
  content = "",
  attr = {}
) {
  const el = document.createElement(type);

  if (className.constructor.name == "Array") {
    className.forEach((cls) => el.classList.add(cls));
  } else {
    el.classList.add(className);
  }

  el.appendChild(document.createTextNode(content));
  Object.keys(attr).forEach((a) => {
    el.setAttribute(a, attr[a]);
  });

  parent.appendChild(el);
  return el;
}
