# Veed

A simple and light weight template for a start page (home page) made using HTML, CSS and JS.

![](./assets/screenshot.png)

## Usage

- Clone this repository.
- Make a new file `services.json` in the project root.
- Fill in the services to `services.json`. (See `services.sample.json` for example).

> **Note:** The folder services is ignored from git. So it is better to add files of new logos and icons to this folder.

### Sample `services.json`
```json
[
  {
    "name": "Cloud",
    "logo": "services/globe.svg",
    "cards": [
      {
        "name": "Nextcloud",
        "description": "nextcloud.com",
        "url": "https://nextcloud.com/",
        "authenticated": true,
        "logo": "services/nextcloud.png"
      }
    ]
  }
]
```